﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi1.Model;
using WebApi1.repository;

namespace WebApi1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private readonly EntityDbContext _context;
        public List<Books> Books;
        private IAuthorRepository _authorRepository;

        public AuthorsController()
        {
            _authorRepository = new AuthorRepository(new EntityDbContext()); 
        }
        //public AuthorsController(IAuthorRepository authorRepository)
        //{
        //    _authorRepository = authorRepository;
        //}

        // GET: api/Authors
        [HttpGet]
        public List<Author> GetAuthor()
        {
            var authordata = _authorRepository.GetAllAuthor();
            return authordata;
        }

        // GET: api/Authors/5
        [HttpGet("{id}")]
        public  Author GetAuthor(int id)
        {
            var author =  _authorRepository.GetAuthorById(id);

            if (author == null)
            {
                //return NotFound();
            }

            return author;
        }

        // PUT: api/Authors/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAuthor(int id, Author author)
        {
            if (id != author.AuthorId)
            {
                return BadRequest();
            }

            _context.Entry(author).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AuthorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Authors
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Author>> PostAuthor(Author author)
        {
            //var author = new Author
            //{
            //    FirstName = "William",
            //    LastName = "Shakespeare",
            //    Book = new List<Books> {
            //        new Books { Title = "Hamlet"},
            //        new Books { Title = "Othello" },
            //        new Books { Title = "MacBeth" }
            //    }
            //};
            //_context.Add(author);
            //_context.SaveChanges();
            //var author1 = new Author { FirstName = "Stephen", LastName = "King" };
            //var books = new List<Books> {
            //    new Books { Title = "It", Author = author1 },
            //    new Books { Title = "Carrie", Author = author1 },
            //    new Books { Title = "Misery", Author = author1 }
            //};
            //_context.AddRange(books);
            //_context.SaveChanges();
            _context.Author.Add(author);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAuthor", new { id  = author.AuthorId }, author);
        }

        // DELETE: api/Authors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Author>> DeleteAuthor(int id)
        {
            var author = _context.Author.Find(1);
            var books = _context.Books.Where(b => EF.Property<int>(b, "AuthorId") == 1);
            foreach (var book in books)
            {
                author.Book.Remove(book);
            }
            _context.Author.Remove(author);
            _context.SaveChanges();


            //var author = await _context.Author.FindAsync(id);
            //if (author == null)
            //{
            //    return NotFound();
            //}

            //_context.Author.Remove(author);
            //await _context.SaveChangesAsync();

            return author;
        }

        private bool AuthorExists(int id)
        {
            return _context.Author.Any(e => e.AuthorId == id);
        }
    }
}
