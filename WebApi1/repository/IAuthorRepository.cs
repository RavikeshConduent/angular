﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi1.Model;

namespace WebApi1.repository
{
    public interface IAuthorRepository : IDisposable
    {
        List<Author> GetAllAuthor();
        Author GetAuthorById(int AuthorId);
        int AddAuthor(Author AuthorEntity);
        int UpdateAuthor(Author AuthorEntity);
        Task<ActionResult<Author>> DeleteAuthor(int AuthorId);
    }
}
