﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi1.Model;

namespace WebApi1.repository
{
    public class AuthorRepository : ControllerBase, IAuthorRepository
    {
        private readonly EntityDbContext _context;

        public AuthorRepository(EntityDbContext context)
        {
            _context = context;
        }
        public int AddAuthor(Author AuthorEntity)
        {
            _context.Author.Add(AuthorEntity);
            _context.SaveChanges();
            
            return 1;
        }


        public void DeleteAuthor(int AuthorId)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<Author> GetAllAuthor()
        {
            //List<Author> getall =  _context.Author.Include(b => b.Book).ToList();
            var getall = _context.Author.FromSqlRaw("SELECT [a].* , [b].* FROM[Author] AS[a] LEFT JOIN[Books] AS[b] ON[a].[AuthorId] = [b].[AuthorId]").OrderBy(b => b.FirstName).ToList();
            return getall;
        }

        public Author GetAuthorById(int AuthorId)
        {
            var author = _context.Author.Find(AuthorId);

            if (author == null)
            {
                // return NotFound();
            }

            return author;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutAuthor(int id, Author author)
        {
            if (id != author.AuthorId)
            {
                return BadRequest();
            }

            _context.Entry(author).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AuthorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        private bool AuthorExists(int id)
        {
            return _context.Author.Any(e => e.AuthorId == id);
        }

        public int UpdateAuthor(Author AuthorEntity)
        {
            throw new NotImplementedException();
        }
    }
}
