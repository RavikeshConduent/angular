﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi1.Model
{

    public class Books
    {
        [Column("BookId")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int BookId { get; set; }
        [Column("Title")]
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        // public int AuthorId { get; set; }
        [ForeignKey("AuthorId")]
        [JsonIgnore]
        public Author Author { get; set; }
        public ICollection<BookCategory> BookCategories { get; set; }

    }
    [NotMapped]
    public class BookDTO
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public int AuthorId { get; set; }
        public Author Author { get; set; }
        public ICollection<BookCategory> BookCategories { get; set; }
    }

}
