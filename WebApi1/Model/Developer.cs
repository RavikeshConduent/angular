﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi1.Model
{
    public class Developer
    {
        [Column("DeveloperId")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int DeveloperId { get; set; }

        [Column("Name")]
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Column("Skills")]
        [Required]
        [StringLength(500)]
        public string Skills { get; set; }

        [ForeignKey("CountryID")]
        public virtual Country Country { get; set; }

    }
}