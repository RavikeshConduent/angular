﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi1.Model
{
    public class EmployeeContactDetail
    {
        [Key]
        [Column("EmpContactId")]
        public int EmpContactId { get; set; }

        [Column("Email")]
        public string Email { get; set; }
        [Column("Mobile")]
        [StringLength(20)]
        public string Mobile { get; set; }

        [Column("Landline")]
        [StringLength(20)]
        public string Landline { get; set; }
        [ForeignKey("EmpId")]
        public Employee Employee { get; set; }


    }
}