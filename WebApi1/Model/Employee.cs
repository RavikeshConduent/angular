﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi1.Model
{
    public class Employee
    {
        [Column("EmpId")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int EmpId { get; set; }
        [Column("FirstName")]
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Column("LastName")]
        [StringLength(50)]
        public string LastName { get; set; }
        [Column("Gender")]
        [Required]
        [StringLength(10)]
        public string Gender { get; set; }
        [Column("Salary")]
        [Required]
        public int Salary { get; set; }
        [Column("DepartmentId")]
        public int DepartmentId { get; set; }
        // Navigation Property
        [ForeignKey("DepartmentId")]
        public Department Department { get; set; }

        [Column("JobId")]
        [ForeignKey("JobId")]
        [StringLength(20)]

        public int JobId { get; set; }

        [Column("Type")]
        [StringLength(50)]
        public string Type { get; set; }

        [Column("CreatedDate")]
        public DateTime CreatedDate { get; set; }

        [Column("CreatedBy")]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        [Column("IsTerminated")]
        public bool IsTerminated { get; set; }

        //[ForeignKey("EmpContactId")]
        //public EmployeeContactDetail EmployeeContactDetail { get; set; }

    }


    [NotMapped]
    public class EmployeeDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DepartmentName { get; set; }
    }
}
