﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi1.Model
{
    public class Department
    {
        // Scalar Properties
        [Column("DepartmentId")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int DepartmentId { get; set; }

        [Column("Name")]
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Column("Location")]
        [StringLength(100)]
        public string Location { get; set; }

        // Navigation Property        
        public List<Employee> Employees { get; set; }

        [Column("CreatedDate")]
        public DateTime CreatedDate { get; set; }

        [Column("CreatedBy")]
        [StringLength(50)]
        public string CreatedBy { get; set; }
    }
}
