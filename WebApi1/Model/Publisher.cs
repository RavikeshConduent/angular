﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi1.Model
{
    public class Publisher
    {
        [Column("PublisherId")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int PublisherId { get; set; }
        [Column("PublisherName")]
        [Required]
        [StringLength(50)]
        public string PublisherName { get; set; }
    }
}