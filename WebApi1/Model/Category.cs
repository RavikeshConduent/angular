﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi1.Model
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public ICollection<BookCategory> BookCategories { get; set; }
    }
    
    public class BookCategory
    {
        public int BookId { get; set; }
        public Books Book { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
